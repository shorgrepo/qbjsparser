<?php

namespace FL\QBJSParser\Model;

use FL\QBJSParser\Exception\Model\RuleGroupConstructionException;

class RuleGroup implements RuleGroupInterface
{
    /**
     * @var \SplObjectStorage
     */
    private $rules;

    /**
     * @var \SplObjectStorage
     */
    private $ruleGroups;

    /**
     * @var string
     */
    private $mode;

    /**
     * @param string $mode
     */
    public function __construct( $mode)
    {
        $this->rules = new \SplObjectStorage();
        $this->ruleGroups = new \SplObjectStorage();

        if (!in_array($mode, self::getDefinedModes())) {
            throw new RuleGroupConstructionException();
        }

        $this->mode = $mode;
    }

    /**
     * @return array
     */
    static public function getDefinedModes ()
    {
        return [
            static::MODE_AND,
            static::MODE_OR,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * {@inheritdoc}
     */
    public function addRule(RuleInterface $rule)
    {
        $this->rules->attach($rule);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeRule(RuleInterface $rule)
    {
        $this->rules->detach($rule);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRuleGroups()
    {
        return $this->ruleGroups;
    }

    /**
     * {@inheritdoc}
     */
    public function addRuleGroup(RuleGroupInterface $ruleGroup)
    {
        $this->ruleGroups->attach($ruleGroup);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeRuleGroup(RuleGroupInterface $ruleGroup)
    {
        $this->ruleGroups->detach($ruleGroup);

        return $this;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }
}
