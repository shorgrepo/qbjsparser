<?php

namespace FL\QBJSParser\Model;


class ProjectionField
{

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $field;

    /**
     * Possibilities for $type come from JSQueryBuilder types:
     * [
     *  'string', 'integer', 'double', 'date', 'time', 'datetime', 'boolean'
     * ].
     *
     * @var string
     */
    private $type;

    /**
     * @param string $id
     * @param string $field
     * @param string $type
     * @param string $operator
     * @param mixed  $value
     */
    public function __construct($id,  $field,  $type)
    {
        $this->id = $id;
        $this->field = $field;
        $this->type = $type;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     * Useful to convert a value (such as @see Rule::$value
     * into PHP variable types and class names.
     *
     * @see http://php.net/manual/en/function.gettype.php
     * @see http://php.net/manual/en/function.get-class.php
     * [
     *  'boolean', 'integer', 'double', 'string', 'array', 'NULL', ClassName::class
     * ]
     */
    public function valueType($value)
    {
        $valueType = gettype($value);
        if (gettype($value) === 'object') {
            return get_class($value);
        }

        return $valueType;
    }
}
