<?php

namespace FL\QBJSParser\Model;

interface RuleInterface
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @return string
     */
    public function getField();

    /**
     * @return string
     */
    public function getType();

    /**
     * @return string
     */
    public function getOperator();

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param $value
     *
     * @return string
     */
    public function valueType($value);
}
