<?php

namespace FL\QBJSParser\Parser\Doctrine;

abstract class JoinPartialParser
{

    /**
     * @var array list of classes to join
     */
    public static $joinedClasses;

    /**
     * @param array $queryBuilderFieldPrefixesToAssociationClasses
     *
     * @return string
     */
    final public static function parse(array $queryBuilderFieldPrefixesToAssociationClasses)
    {
        // Define static::$joinedClasses;
        static::addToJoinedClasses();

        $joinString = '';
        if (!empty(static::$joinedClasses)) {
            foreach ($queryBuilderFieldPrefixesToAssociationClasses as $queryBuilderPrefix => $associationClass) {
                if (array_key_exists($queryBuilderPrefix, static::$joinedClasses)) {
                    $joinPart = sprintf(
                        ' %s.%s ',
                        SelectPartialParser::OBJECT_WORD,
                        $queryBuilderPrefix
                    );
                    $joinString .= sprintf(
                        ' LEFT JOIN %s %s ',
                        StringManipulator::replaceAllDotsExceptLast($joinPart),
                        StringManipulator::replaceAllDots($joinPart)
                    );
                }
            }
        }
        return $joinString;
    }

    /**
     * @return string
     */
    final public static function addToJoinedClasses()
    {
        if(is_array(WherePartialParser::$queryBuilderFieldsToPropertiesUsedInWhere )) {
            foreach (WherePartialParser::$queryBuilderFieldsToPropertiesUsedInWhere as $queryBuilderFieldString => $propertyString) {
                static::popThenAddClass($propertyString);
            }
        }

        if(is_array(SelectPartialParser::$queryBuilderFieldsToPropertiesUsedInSelect)) {
            foreach (SelectPartialParser::$queryBuilderFieldsToPropertiesUsedInSelect as $queryBuilderFieldString => $propertyString) {
                static::popThenAddClass($propertyString);
            };
        }

    }

    /**
     * @param string $propertyString
     */
    final private static function popThenAddClass($propertyString)
    {
        // Get property as array
        $arrProperty = explode(".", $propertyString);

        // if no prefix : entity is the main object
        if (count($arrProperty) == 1) {
            return;
        }

        // Remove last item :
        array_pop($arrProperty);

        // Redefine property with others items
        if (count($arrProperty) > 1) {
            $strProperty = implode(".", $arrProperty);
        } else {
            $strProperty = $arrProperty[0];
        }

        // Add property to joined classe
        static::$joinedClasses[$strProperty] = 1;

        // Recursively add classes
        if (!empty($strProperty)) {
            static::popThenAddClass($strProperty);
        }

    }

}
