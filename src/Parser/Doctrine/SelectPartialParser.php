<?php

namespace FL\QBJSParser\Parser\Doctrine;

use FL\QBJSParser\Model\ProjectionField;

abstract class SelectPartialParser
{
    const OBJECT_WORD = 'object';

    /**
     * @var array
     */
    public static $queryBuilderFieldsToPropertiesUsedInSelect;

    /**
     * @param array $mappingProjectionFields
     * @param array $fieldPrefixesToClasses
     *
     * @return string
     */
    final public static function parse(
        array $mappingProjectionFields = [],
        array $distinctProperty = null,
        array $projectionFieldsToProperties = []
    ) {
        // Define SELECT DISTINCT directive
        if (empty($distinctProperty)) {
            $distinctString = "";
        } else {
            $distinctString = " DISTINCT(".self::OBJECT_WORD.'.'.$projectionFieldsToProperties[$distinctProperty["systSrcPropertyNamespace"]].")";
            $distinctString .= " AS distinct__".str_replace(
                    '.',
                    '__',
                    StringManipulator::replaceAllDotsExceptLast(
                        self::OBJECT_WORD.'.'.$distinctProperty["systSrcPropertyNamespace"]
                    )
                );
        }

        // Check parameters
        if ((empty($mappingProjectionFields) && empty($distinctProperty)) || empty($projectionFieldsToProperties)) {
            return "SELECT ".$distinctString.' '.static::OBJECT_WORD.' ';
        }

        /** @var ProjectionField $field */
        $fieldsString = "";
        foreach ($mappingProjectionFields as $field => $value) {
            if (isset($projectionFieldsToProperties[$field])) {

                // Prepare array of fields used in Select
                static::$queryBuilderFieldsToPropertiesUsedInSelect[$field] = $projectionFieldsToProperties[$field];

                $fieldsString = empty($fieldsString) ? $fieldsString : $fieldsString.", ";
                $fieldsString .= StringManipulator::replaceAllDotsExceptLast(
                    self::OBJECT_WORD.'.'.$projectionFieldsToProperties[$field]
                );
                $fieldsString .= " AS ".str_replace(
                        '.',
                        '__',
                        StringManipulator::replaceAllDotsExceptLast(self::OBJECT_WORD.'.'.$field)
                    );
            }
        };

        return (empty($distinctString)) ?
            "SELECT ".$fieldsString :
            "SELECT ".$distinctString.", ".$fieldsString;
    }


}
