<?php

namespace FL\QBJSParser\Parsed;

abstract class AbstractParsedRuleGroup
{
    /**
     * @return string
     */
    abstract public function getQueryString();

    /**
     * @return array
     */
    abstract public function getParameters();

    /**
     * The class name of the objects this ParsedRuleGroup is querying.
     *
     * @return string
     */
    abstract public function getClassName();

    /**
     * Manipulate the query string with search and replace.
     *
     * @param string $search
     * @param string $replace
     * @param string $appendToEndIfNotFound
     *
     * @return AbstractParsedRuleGroup
     */
    abstract public function copyWithReplacedString( $search,  $replace,  $appendToEndIfNotFound);

    /**
     * Manipulate the query string with regex search and replace.
     *
     * @param string $pattern
     * @param string $replace
     * @param string $appendToEndIfNotFound
     *
     * @return AbstractParsedRuleGroup
     */
    abstract public function copyWithReplacedStringRegex( $pattern,  $replace,  $appendToEndIfNotFound);
}
